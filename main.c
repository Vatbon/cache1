#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define COUNT 8 
#define COUNTI 32
#pragma GCC push_options
#pragma GCC optimize ("O0")

size_t run(size_t* arr, size_t size){
	union ticks{
		unsigned long long t64;
		struct s32{ long th, tl;} t32;
	} start, end;
	size_t l;
	register size_t i, k;
	size_t tmp = UINT_MAX;
	double min;

	for (k = 0 , i = 0; i < size; i++){
		k = arr[k];
	}

	for (l = 0; l < COUNT; l++){
		asm("rdtsc\n":"=a"(start.t32.th),"=d"(start.t32.tl));
		for (k = 0, i = 0; i < size * COUNTI; i++){
			k = arr[k];
		}
		asm("rdtsc\n":"=a"(end.t32.th),"=d"(end.t32.tl));
		tmp = (tmp > end.t64 - start.t64) ? (end.t64 - start.t64) : tmp;
	}
	min = tmp / (size * COUNTI);
	return min;
}
#pragma GCC pop_options

int main(){
	unsigned long long i, l, k, z, size = 128;
	size_t* forw;
	size_t* ran;
	size_t* rev;
	for (z = 0;(size*sizeof(size_t)/1024 < 64000); z++){

		//FORWARD
		forw = (size_t*)malloc(size*sizeof(size_t));

		for (i = 0; i < size; i++){
			forw[i] = i + 1;
		}
		forw[size - 1] = 0;

		printf("Time %lld KB taken: %d\n", size*sizeof(size_t)/1024 , run(forw, size));
		free(forw);
		//REVERSE
		rev = (size_t*)malloc(size*sizeof(size_t));
		for (i = size; i > 1; i--){
			rev[i - 1] = i - 2;
		}
		rev[0] = size - 1;


		printf("Time %lld KB taken: %d\n", size*sizeof(size_t)/1024 , run(rev, size));
		free(rev);
		//RANDOM 
		ran = (size_t*)malloc(size*sizeof(size_t));
		for (i = 0; i < size; i++)
			ran[i] = 0;
		srand(1);	
		for (k = 0, l = 0, i = 0; i < size - 1; i++){

			k = rand() % size;
			while (ran[k] != 0)
				k = ((((size_t)rand())<<4) | (size_t)rand()) % size;
			ran[l] = k;
			l = k;
		}

		/*for (i = 0; i < size/2; i++){
			ran[i] = size - 1 - i ;
		}
		for (i = size/2; i < size; i++){
			ran[i] = size - 2 - i ;
		}
		ran[size - 1]= (size - 1) / 2;
*/

		printf("Time %lld KB taken: %d\n", size*sizeof(size_t)/1024 , run(ran, size));
		free(ran);
	/*	if (z < 32){
			if(z==0)
				size=0;
		size += 128*sizeof(size_t)/8;
		}
		else if(z < 64){
			size += 128*sizeof(size_t)*32;
		}
		else{
			size += 128*sizeof(size_t)*64;
		}*/
		size += 1024/4;
		if (size*sizeof(size_t)/1024 > 256 ){
			size+= 4096/2;
		}
		if(size*sizeof(size_t)/1024 > 512 ){
			size+= 4096*2;
		}
		if(size*sizeof(size_t)/1024 > 1024 )
			size+= 4096*2;
		printf("\n");
	}
	return 0;
}
